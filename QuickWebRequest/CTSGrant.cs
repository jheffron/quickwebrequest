﻿using System.Threading;

namespace QuickWebRequest
{
    // philosophy:
        // Timeouts are useful with web requests, so that we don't wait too long for any given request to complete
        // We can use CancellationTokenSources to grant special CancellationTokens with a timeout value attached
        // A special static class to generate unique tokens is needed as a token will become invalid once a cancellation has fired

        // Notably, we have a host token generating child tokens for in case we wish to end the HttpClient and all requests at once
        // This functionality isn't currently implemented, but is available

    internal static class CTSGrant
    {
        private static CancellationTokenSource root = new CancellationTokenSource();
        private static System.TimeSpan t = System.TimeSpan.FromSeconds(10); //default timeout
        internal static CancellationToken GrantToken()
        {
            // create child CTS from our 'root' CTS
            CancellationTokenSource grant = CancellationTokenSource.CreateLinkedTokenSource(root.Token);

            grant.CancelAfter(t); // set timeout value from stored TimeSpan

            return grant.Token; // return the token from the CTS, but not the CTS itself
        }

        internal static CancellationToken GrantToken(System.TimeSpan Time)
        {
            CancellationTokenSource grant = CancellationTokenSource.CreateLinkedTokenSource(root.Token);
            grant.CancelAfter(Time); // set timeout from input

            return grant.Token;
            // This performs the same as above, but allow the user to pass an individual timeout value for the token
        }

        internal static void setTimeout(System.TimeSpan Time)
        {
            t = Time;
        }
    }
}
