﻿namespace QuickWebRequest
{
    // Request Message Information Base object type
    public abstract class RMIBase
    {
        protected System.Collections.Generic.Dictionary<string, string> data;

        public RMIBase()
        {
            this.data = new System.Collections.Generic.Dictionary<string, string>();
        }

        public System.Collections.Generic.Dictionary<string, string> Data
        {
            get { return this.data; }
        }

        public RMIBase(RMIBase Clone)
        {
            this.data = Clone.data;
        }

        public RMIBase(System.Collections.Generic.Dictionary<string, string> Data)
        {
            this.data = Data;
        }

        public RMIBase Set(string Key, string Value)
        {
            this.data[Key] = Value;
            return this;
        }

        public RMIBase Remove(string Key)
        {
            if (this.data.ContainsKey(Key)) this.data.Remove(Key);
            return this;
        }

        public RMIBase Clear()
        {
            this.data.Clear();
            return this;
        }

        abstract internal void Port(System.Net.Http.HttpRequestMessage RequestMessage);

        
    }
}
