﻿namespace QuickWebRequest
{
    public class HeaderData : RMIBase
    {
        internal override void Port(System.Net.Http.HttpRequestMessage RequestMessage)
        {
            foreach (System.Collections.Generic.KeyValuePair<string, string> header in this.data)
            {
                RequestMessage.Headers.Add(header.Key, header.Value);
            }
        }
    }
}