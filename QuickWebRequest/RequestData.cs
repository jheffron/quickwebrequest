﻿
namespace QuickWebRequest
{
    public class RequestData : RMIBase
    {
        internal override void Port(System.Net.Http.HttpRequestMessage RequestMessage)
        {
            RequestMessage.Content = new System.Net.Http.FormUrlEncodedContent(this.data);
        }
    }
}
