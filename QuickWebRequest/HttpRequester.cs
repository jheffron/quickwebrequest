﻿using System.Net.Http;

namespace QuickWebRequest
{
    public class HttpRequester
    {
        public HttpRequester()
        {
            Controller.headers = new HeaderData();
        }

        public HttpRequester(HeaderData Data)
        {
            Controller.headers = Data;
        }

        // General Methods

        public HeaderData Headers
        {
            get { return Controller.headers; }
            set { Controller.headers = value; }
        }

        public void SetTimeout(System.TimeSpan Time)
        {
            CTSGrant.setTimeout(Time);
        }

        public void ClearHeaders()
        {
            Controller.headers.Clear();
        }

        // Standard requests

        public string Get(string URL)
        {
            return Controller.RequestString(HttpMethod.Get, new System.Uri(URL));
        }

        public string Get(System.Uri URL)
        {
            return Controller.RequestString(HttpMethod.Get, URL);
        }

        public string Head(string URL)
        {
            return Controller.RequestString(HttpMethod.Head, new System.Uri(URL));
        }

        public string Head(System.Uri URL)
        {
            return Controller.RequestString(HttpMethod.Head, URL);
        }

        public string Delete(string URL)
        {
            return Controller.RequestString(HttpMethod.Delete, new System.Uri(URL));
        }

        public string Delete(System.Uri URL)
        {
            return Controller.RequestString(HttpMethod.Delete, URL);
        }

        public string Options(string URL)
        {
            return Controller.RequestString(HttpMethod.Options, new System.Uri(URL));
        }

        public string Options(System.Uri URL)
        {
            return Controller.RequestString(HttpMethod.Options, URL);
        }

        public string Trace(string URL)
        {
            return Controller.RequestString(HttpMethod.Trace, new System.Uri(URL));
        }

        public string Trace(System.Uri URL)
        {
            return Controller.RequestString(HttpMethod.Trace, URL);
        }

        public string Post(string URL, RequestData Content)
        {
            return Controller.RequestString(HttpMethod.Post, new System.Uri(URL), Content);
        }

        public string Post(System.Uri URL, RequestData Content)
        {
            return Controller.RequestString(HttpMethod.Post, URL, Content);
        }

        public string Put(string URL, RequestData Content)
        {
            return Controller.RequestString(HttpMethod.Put, new System.Uri(URL), Content);
        }

        public string Put(System.Uri URL, RequestData Content)
        {
            return Controller.RequestString(HttpMethod.Put, URL, Content);
        }

        // Extended/Transformative requests

        public System.Drawing.Image GetImage(string URL)
        {
            return System.Drawing.Image.FromStream(Controller.RequestStream(HttpMethod.Get, new System.Uri(URL)));
        }

        public System.Drawing.Image GetImage(System.Uri URL)
        {
            return System.Drawing.Image.FromStream(Controller.RequestStream(HttpMethod.Get, URL));
        }

        public System.IO.Stream GetRaw(string URL)
        {
            return Controller.RequestStream(HttpMethod.Get, new System.Uri(URL));
        }

        public System.IO.Stream GetRaw(System.Uri URL)
        {
            return Controller.RequestStream(HttpMethod.Get, URL);
        }

        public System.IO.Stream PostRaw(string URL, RequestData Content)
        {
            return Controller.RequestStream(HttpMethod.Post, new System.Uri(URL), Content);
        }

        public System.IO.Stream PostRaw(System.Uri URL, RequestData Content)
        {
            return Controller.RequestStream(HttpMethod.Post, URL, Content);
        }

        public System.IO.Stream PutRaw(string URL, RequestData Content)
        {
            return Controller.RequestStream(HttpMethod.Put, new System.Uri(URL), Content);
        }

        public System.IO.Stream PutRaw(System.Uri URL, RequestData Content)
        {
            return Controller.RequestStream(HttpMethod.Put, URL, Content);
        }
    }
}