﻿using System.Net.Http;
using System.Threading.Tasks;

namespace QuickWebRequest
{

    internal static class Controller
    {

        private static readonly HttpClient client = new HttpClient();

        // This may look odd at first, but this static HttpClient class with instantiated wrappers for header data is very intentional
        // https://docs.microsoft.com/en-us/dotnet/api/system.net.http.httpclient?view=netframework-4.7.2#remarks

        internal static HeaderData headers;

        // For the following methods:

        // We run this in a try with a catch for an InnerException as an aggregate exception would get called from the Task
        // It would be more valuable to know what happened inside it rather than the aggregate itself

        internal static string RequestString(HttpMethod method, System.Uri page)
        {
            try
            {
                return Task.Run(async () => await StringStaticRequest(method, page)).Result;
            }
            catch (System.AggregateException e)
            {
                throw e.InnerException;
            }
        }

        internal static string RequestString(HttpMethod method, System.Uri page, RequestData data)
        {
            try
            {
                return Task.Run(async () => await StringContentRequest(method, page, data)).Result;
            }
            catch (System.AggregateException e)
            {
                throw e.InnerException;
            }
        }

        internal static System.IO.Stream RequestStream(HttpMethod method, System.Uri page)
        {
            try
            {
                return Task.Run(async () => await StreamStaticRequest(method, page)).Result;
            }
            catch (System.AggregateException e)
            {
                throw e.InnerException;
            }
        }

        internal static System.IO.Stream RequestStream(HttpMethod method, System.Uri page, RequestData data)
        {
            try
            {
                return Task.Run(async () => await StreamContentRequest(method, page, data)).Result;
            }
            catch (System.AggregateException e)
            {
                throw e.InnerException;
            }
        }

        // For the following methods:

            // We make a request and attach our configured request headers to that request
            // If our request type requires additional data, that is also to be supplied as a parameter

            // From these, we pull a response with our request and a fresh timeout cancellationToken
            // We then pull the desired value as 'result'
            // We then fire the Dispose() method for the request and response as we are done with them

            // Finally, we are catching TaskCancelledExceptions and converting to TimeoutExceptions
            // This is due to resolving ambiguity with HttpClient
            // We have removed the second case for such an exception to fire, leaving only a timeout
    
        // Extra notes for content requests:
       
            // content assignment goes before assigning headers, as headers are part of the content
        
            // we also need to override expect 100-continue as by default, this is passed in the headers
            // it breaks many websites, so it shall be disabled by default
            // the user can simply readd it as a header if they wish

        private static async Task<string> StringStaticRequest(HttpMethod method, System.Uri page)
        {
            var request = new HttpRequestMessage(method, page);
            headers.Port(request);
            try
            {
                var response = await client.SendAsync(request, CTSGrant.GrantToken());
                string result;

                // special logic for head requests and others
                switch (method.Method)
                {
                    case "HEAD":
                        result = response.Headers.ToString();
                        break;

                    case "DELETE":
                        result = response.StatusCode.ToString();
                        
                        break;

                    case "OPTIONS":
                        if (response.Headers.Contains("Access-Control-Allow-Methods"))
                            result = string.Join("\n", response.Headers.GetValues("Access-Control-Allow-Methods"));
                        else
                            result = "";
                        break;

                    case "TRACE":
                        result = response.Headers.ToString();
                        break;

                    default:
                        result = response.Content.ReadAsStringAsync().Result;
                        break;

                }

                request.Dispose();
                response.Dispose();

                return result;
            }
            catch (TaskCanceledException)
            {
                throw new System.TimeoutException();
            }
        }

        private static async Task<System.IO.Stream> StreamStaticRequest(HttpMethod method, System.Uri page)
        {
            var request = new HttpRequestMessage(method, page);
            headers.Port(request);

            try
            {
                var response = await client.SendAsync(request, CTSGrant.GrantToken());
                System.IO.Stream result = response.Content.ReadAsStreamAsync().Result;

                request.Dispose();
                response.Dispose();

                return result;
            }
            catch (TaskCanceledException)
            {
                throw new System.TimeoutException();
            }
        }

        private static async Task<string> StringContentRequest(HttpMethod method, System.Uri page, RequestData content)
        {
            var request = new HttpRequestMessage(method, page);
            content.Port(request);
            request.Headers.ExpectContinue = false;
            headers.Port(request);

            try
            {
                var response = await client.SendAsync(request, CTSGrant.GrantToken());
                string result = response.Content.ReadAsStringAsync().Result;

                request.Dispose();
                response.Dispose();

                return result;
            }
            catch (TaskCanceledException)
            {
                throw new System.TimeoutException();
            }
        }

        private static async Task<System.IO.Stream> StreamContentRequest(HttpMethod method, System.Uri page, RequestData content)
        {
            var request = new HttpRequestMessage(method, page);
            content.Port(request);
            request.Headers.ExpectContinue = false;
            headers.Port(request);

            try
            {
                var response = await client.SendAsync(request, CTSGrant.GrantToken());
                System.IO.Stream result = response.Content.ReadAsStreamAsync().Result;

                request.Dispose();
                response.Dispose();

                return result;
            }
            catch (TaskCanceledException)
            {
                throw new System.TimeoutException();
            }
        }
    }
}
